class Fixnum
    @@numwords = {
        0 => "zero",
        1 => "one",
        2 => "two",
        3 => "three",
        4 => "four",
        5 => "five",
        6 => "six",
        7 => "seven",
        8 => "eight",
        9 => "nine",
        10 => "ten",
        11 => "eleven",
        12 => "twelve",
        13 => "thirteen",
        14 => "fourteen",
        15 => "fifteen",
        16 => "sixteen",
        17 => "seventeen",
        18 => "eighteen",
        19 => "nineteen",
        20 => "twenty",
        30 => "thirty",
        40 => "forty",
        50 => "fifty",
        60 => "sixty",
        70 => "seventy",
        80 => "eighty",
        90 => "ninety"
    }
    
    def in_words
        return @@numwords[self] if @@numwords.has_key?(self) == true
        name = []
        number = self
        [12, 9, 6, 3, 0].each do |power|
            x = number / 10 ** power
            case power
            when 12
                next if x < 1
                name << hundreds_names(x)
                name << "trillion"
                number = number - (x * 10**power)
            when 9
                next if x < 1
                name << hundreds_names(x)
                name << "billion"
                number = number - (x * 10**power)
            when 6
                next if x < 1
                name << hundreds_names(x)
                name << "million"
                number = number - (x * 10**power)
            when 3
                next if x < 1
                name << hundreds_names(x)
                name << "thousand"
                number = number - (x * 10**power)
            when 0
                next if x < 1
                name << hundreds_names(x)
            end
        end
                
        name.join(" ")
    end
        
    private
    
    def hundreds_names(n)
        num = n.to_s.chars
        name = []
        num.each_with_index do |i, idx|
            ones_place = true
            if num.length == 3 && idx == 0
                name << @@numwords[i.to_i] 
                name << "hundred"
            elsif (num.length == 3 && idx == 1) || (num.length == 2 && idx == 0)
                next if i.to_i == 0
                if i.to_i < 2
                    name << @@numwords[ num[idx, 2].join.to_i ]
                    ones_place = false
                end
                break if ones_place == false
                name << @@numwords[i.to_i * 10]
            else
                break if i.to_i == 0
                name << @@numwords[i.to_i]
            end
        end
        
        name.join(" ")
    end
                
end
